# Hpclient
*forked from [yorick-dumet/docker-horaires](https://gitlab.com/yorick-dumet/docker-horaires/-/blob/laravel/app/app/Models/Soap/SoapInterfaces/IPromotion.php)*
> php client for soap hyperplanning service

## Install 
```php
composer config repositories.hpclient vcs https://gitlab.com/dendev/hpclient.git
composer require dendev/hpclient
```

## Test
**set config file with hyperplaning login and values**
```php
cp tests/config.sample.php tests/config.php
vim tests/config.php
```

run all tests
```bash
phpunit 
```

run specific tests
```bash
phpunit tests/HpConnectTest.php
```

run specific test
```bash
phpunit --filter testToutesLesPromotions 
```

## Try
You can use all methods in cli interface
```php
php cli
```

## Doc
install [doctum](https://github.com/code-lts/doctum)
```bash
curl -O https://doctum.long-term.support/releases/latest/doctum.phar
php doctum.phar update doctum.php
google-chrome ./docs/build/index.html
```
