<?php

namespace Operations;

use Dendev\Hpclient\HPClient;
use PHPUnit\Framework\TestCase;


final class EnseignantTest extends TestCase
{
    private static $_client;
    private static $_config;

    public static function setUpBeforeClass(): void
    {
        $config = include('./tests/config.php');

        self::$_config = $config;
        self::$_client = new HPClient($config['wsdl'], $config['login'], $config['password'], $config['location'], $config['trace']);
    }

    public function testTousLesEnseignants()
    {
        $data = self::$_client->tousLesEnseignants();
        $this->assertNotEmpty($data);
    }

    public function testNomEnseignant()
    {
        $key = self::$_config['key_teachers'][0];
        $name = self::$_config['name_teachers'][0];

        // ok
        $data = self::$_client->NomEnseignant($key);
        $this->assertEquals($name, $data);

        // ko
        $data = self::$_client->NomEnseignant('NOTFOUND');
        $this->assertFalse($data);
    }

    public function testPrenomEnseignant()
    {
        $key = self::$_config['key_teachers'][0];
        $firstname = self::$_config['firstname_teachers'][0];

        // ok
        $data = self::$_client->PrenomEnseignant($key);
        $this->assertEquals($firstname, $data);

        // ko
        $data = self::$_client->PrenomEnseignant('NOTFOUND');
        $this->assertFalse($data);
    }

    public function testCodeEnseignant()
    {
        $key = self::$_config['key_teachers'][0];
        $code = self::$_config['code_teachers'][0];

        // ok
        $data = self::$_client->codeEnseignant($key);
        $this->assertEquals($code, $data);
    }

    public function testIcalEnseignant()
    {
        $key = self::$_config['key_teachers'][0];

        // ok
        $data = self::$_client->icalEnseignant($key, 52);
        $this->assertNotEmpty($data);
    }

    public function testNomsTableauEnseignants()
    {
        $keys = self::$_config['key_teachers'];
        $names = self::$_config['name_teachers'];

        $data = self::$_client->nomsTableauEnseignants($keys);
        $this->assertEquals($names[0], $data[0]);
        $this->assertEquals($names[1], $data[1]);
        $this->assertEquals($names[2], $data[2]);
        $this->assertNotEmpty($data);
    }

    public function testCodesTableauEnseignants()
    {
        $keys = self::$_config['key_teachers'];
        $codes = self::$_config['code_teachers'];

        $data = self::$_client->codesTableauEnseignants($keys);
        $this->assertEquals($codes[0], $data[0]);
        $this->assertEquals($codes[1], $data[1]);
        $this->assertEquals($codes[2], $data[2]);
        $this->assertNotEmpty($data);
    }
}
