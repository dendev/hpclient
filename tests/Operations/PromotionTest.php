<?php

namespace Operations;

use Dendev\Hpclient\HPClient;
use PHPUnit\Framework\TestCase;


final class PromotionTest extends TestCase
{
    private static $_client;
    private static $_config;

    public static function setUpBeforeClass(): void
    {
        $config = include('./tests/config.php');

        self::$_config = $config;
        self::$_client = new HPClient($config['wsdl'], $config['login'], $config['password'], $config['location'], $config['trace']);
    }

    public function testToutesLesPromotions()
    {
        $data = self::$_client->toutesLesPromotions();
        $this->assertNotEmpty($data);
    }

    public function testClePromotionEstValide()
    {
        $key = self::$_config['key_promotions'][0];

         $data = self::$_client->clePromotionEstValide($key);
        $this->assertTrue($data);

        $data = self::$_client->clePromotionEstValide(-1);
        $this->assertFalse($data);
    }

    public function testCodesTableauDePromotions()
    {
        $data = self::$_client->codesTableauDePromotions();
        $this->assertNotEmpty($data);
    }

    public function testNomsTableauPromotions()
    {
        $keys = self::$_config['key_promotions'];
        $names = self::$_config['name_promotions'];

        // ok
        $data = self::$_client->nomsTableauDePromotions($keys);
        $this->assertNotEmpty($data);
        $this->assertContains($names[0], $data);
        $this->assertContains($names[1], $data);

        // ko
        $data = self::$_client->nomsTableauDePromotions([-1]);
        $this->assertFalse($data);
    }

    public function testNomPromotion()
    {
        $key = self::$_config['key_promotions'][0];
        $name = self::$_config['name_promotions'][0];

        $data = self::$_client->nomPromotion($key);
        $this->assertEquals($name, $data);
    }

    public function testCodePromotion()
    {
        $key = self::$_config['key_promotions'][0];

        $data = self::$_client->codePromotion($key);
        $this->assertNotFalse($data);
    }

    public function testAccederPromotionParNomEtCode()
    {
        $name = self::$_config['name_promotions'][0];
        $code = self::$_config['code_promotions'][0];
        $key = self::$_config['key_promotions'][0];

        $data = self::$_client->accederPromotionParNomEtCode($name, $code);
        $this->assertEquals($data, $key);
    }

    public function testIcalPromotion()
    {
        $key = self::$_config['key_promotions'][0];

        // ok
        $data = self::$_client->icalPromotion($key, 52);
        $this->assertNotEmpty($data);
    }
}
