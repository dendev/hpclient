<?php

namespace Operations;

use Dendev\Hpclient\HPClient;
use PHPUnit\Framework\TestCase;


final class SalleTest extends TestCase
{
    private static $_client;
    private static $_config;

    public static function setUpBeforeClass(): void
    {
        $config = include('./tests/config.php');

        self::$_config = $config;
        self::$_client = new HPClient($config['wsdl'], $config['login'], $config['password'], $config['location'], $config['trace']);
    }

    public function testToutesLesSalles()
    {
        $data = self::$_client->toutesLesSalles();
        $this->assertNotEmpty($data);
    }

    public function testNomSalle()
    {
        $key = self::$_config['key_salles'][0];
        $name = self::$_config['name_salles'][0];

        // ok
        $data = self::$_client->NomSalle($key);
        $this->assertEquals($name, $data);

        // ko
        $data = self::$_client->NomSalle('NOTFOUND');
        $this->assertFalse($data);
    }

    public function testNomsTableauDeSalles()
    {
        $keys = self::$_config['key_salles'];
        $names = self::$_config['name_salles'];

        // ok
        $data = self::$_client->nomsTableauDeSalles($keys);
        $this->assertNotEmpty($data);
        $this->assertContains($names[0], $data);
        $this->assertContains($names[1], $data);

        // ko
        $data = self::$_client->nomsTableauDeSalles([-1]);
        $this->assertFalse($data);
    }

    public function testIcalSalle()
    {
        $key = self::$_config['key_salles'][0];

        // ok
        $data = self::$_client->icalSalle($key, 52);
        $this->assertNotEmpty($data);
    }

    /*
    public function testNomToutesLesSalles()
    {
        $names = self::$_client->nomToutesLesSalles();
        dd( $names);
    }
    */
}
