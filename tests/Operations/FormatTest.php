<?php

namespace Operations;

use Dendev\Hpclient\HPClient;
use PHPUnit\Framework\TestCase;


final class FormatTest extends TestCase
{
    private static $_client;
    private static $_config;

    public static function setUpBeforeClass(): void
    {
        $config = include('./tests/config.php');

        self::$_config = $config;
        self::$_client = new HPClient($config['wsdl'], $config['login'], $config['password'], $config['location'], $config['trace']);
    }

    public function testFormatTexteIcal()
    {
        $data = self::$_client->formatTexteIcal();
        $this->assertIsInt($data);
    }

    public function testFormatTexteIcalEnseignant()
    {
        $data = self::$_client->formatTexteIcalEnseignant();
        $this->assertIsInt($data);
    }
}
