<?php
use Dendev\Hpclient\Soap\HPConnect;
use PHPUnit\Framework\TestCase;

final class HPConnectTest extends TestCase
{
    public function testConstruct()
    {
        $config = include('./tests/config.php');

        $connect = new HPConnect($config['wsdl'], $config['login'], $config['password'], $config['location'], $config['trace']);
        $this->assertInstanceOf(SoapClient::class, $connect->client);
    }
}
