<?php
namespace Dendev\Hpclient;

use Dendev\Hpclient\Operations\Enseignant;
use Dendev\Hpclient\Operations\Format;
use Dendev\Hpclient\Operations\Promotion;
use Dendev\Hpclient\Operations\Salle;
use Dendev\Hpclient\Soap\HPConnect;

class HPClient extends HPConnect
{
    use Salle;
    use Enseignant;
    use Promotion;
    use Format;

    public function __construct($wsdl, $login, $password, $location, $trace = true)
    {
        parent::__construct($wsdl, $login, $password, $location, $trace);
    }
}
