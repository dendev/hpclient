<?php

namespace Dendev\Hpclient\Operations;

use Dendev\Hpclient\Traits\Util;

/**
 * Travail avec les webservices enseignant d'hyperplanning
 */
Trait Enseignant
{
    use Util;

    /**
     * Retourne le code HP de l'enseignant dont la clé est fournis.
     * @param $key clé HP de l'enseignant
     * @return string|false le code de l'enseignant ex DEVDE
     */
    public function codeEnseignant($key)
    {
        try
        {
            $data = $this->client->CodeEnseignant($key);
        }
        catch (\SoapFault $e)
        {
            $data = false;
        }

        return $data;
    }


    /**
     * Retourne le code HP des enseignants dont la clé est fournis.
     * @param $keys array clés HP des enseignants
     * @return string|false le code de l'enseignant ex DEVDE
     */
    public function codesTableauEnseignants($key)
    {
        try
        {
            $data = $this->client->CodesTableauDEnseignants($key);
        }
        catch (\SoapFault $e)
        {
            $data = false;
        }

        return $data;
    }

    /**
     * Retourne l'ical d'un enseignant
     *
     * @param $key clé HP de l'enseignant
     * @param $nb_weeks int du nombre de semaines à récupérer
     * @param $with array avec les données à inclue en plus
     * @return string|false contenu de l'ical
     */
    public function icalEnseignant($key, $nb_weeks = 25, $with = false)
    {
        if( ! $with )
            $with = ["fiAvecCoursAnnules", "fiAvecDateSeances", "fiAvecType", "fiAvecPonderation", "fiAvecMemo", "fiAvecEffectif", "fiAvecSites"];

        $nb_weeks = $this->format_nb_weeks($nb_weeks);

        try
        {
            $ical = $this->client->IcalEnseignant( $key,
                $nb_weeks,
                $with,
                $this->client->FormatTexteICAL(false, false, true),
                $this->client->FormatTexteICALEnseignant(false, true, true, false, false, false),
                $this->client->FormatTexteICAL(false, false, true),
                $this->client->FormatTexteICAL(true, false, false),
                $this->client->FormatTexteICAL(true, false, false),
                $this->client->FormatTexteICAL(true, false, false),
                $this->client->FormatTexteICAL(false, false, true));
        }
        catch(\SoapFault $e)
        {
            $ical = false;
        }

        return $ical;
    }

    /**
     * Retourne le nom de l'enseignant dont la clé HP est fournis
     *
     * @param $key clé HP de l'enseignant
     * @return string|false return le nom HP
     */
    public function nomEnseignant($key)
    {
        try
        {
            $data = $this->client->NomEnseignant($key);
        }
        catch (\SoapFault $e)
        {
            $data = false;
        }

        return $data;
    }

    /**
     * Retourne le nom desenseignants dont la clé HP est fournis
     *
     * @param $key clé HP de l'enseignant
     * @return string|false return les noms
     */
    public function nomsTableauEnseignants($keys)
    {
        try
        {
            $data = $this->client->NomsTableauDEnseignants($keys);
        }
        catch (\SoapFault $e)
        {
            $data = false;
        }

        return $data;
    }

    /**
     * Retourne le prénom de l'enseignant dont la clé HP est fournis
     *
     * @param $key clé HP de l'enseignant
     * @return string|false return le prénom HP
     */
    public function prenomEnseignant($key)
    {
        try
        {
            $data = $this->client->PrenomEnseignant($key);
        }
        catch (\SoapFault $e)
        {
            $data = false;
        }

        return $data;
    }

    /**
     * Retourne la clé HP de tous les enseignants existant
     * @return array|false clés des enseignants
     */
    public function tousLesEnseignants()
    {
        try
        {
            $data = $this->client->TousLesEnseignants();
        }
        catch (\SoapFault $e)
        {
            $data = false;
        }

        return $data;
    }
}

