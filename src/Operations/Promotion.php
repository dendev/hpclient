<?php

namespace Dendev\Hpclient\Operations;


use Dendev\Hpclient\Traits\Util;

/**
 * Travail avec les webservices promotion d'hyperplanning
 */
Trait Promotion
{
    use Util;

    /**
     * Permet d'obtenir la clé d'une promotion par le nom et son code
     * @param $name nom HP
     * @param $code code HP
     * @return int clé HP
     */
    public function accederPromotionParNomEtCode($name, $code)
    {
        try
        {
            $data = $this->client->AccederPromotionParNomEtCode($name, $code);
        }
        catch (\SoapFault $e)
        {
            $data = false;
        }

        return $data;
    }

    /**
     * Indique si la clé fournis est valide
     * @param $key
     * @return boolean true si est valide
     */
    public function clePromotionEstValide($key)
    {
        try
        {
            $data = $this->client->ClePromotionEstValide($key);
        }
        catch (\SoapFault $e)
        {
            $data = false;
        }

        return $data;
    }

    /**
     * Retourne les codes des promotions
     * @return array tableau avec les codes des promotions
     */
    public function codesTableauDePromotions()
    {
        try
        {
            $data = $this->client->CodesTableauDePromotions();
        }
        catch (\SoapFault $e)
        {
            $data = false;
        }

        return $data;
    }

    /**
     * Fournis le code de la promotion depuis sa clé
     *
     * @param $key clé HP
     * @return int|false la clé HP
     */
    public function codePromotion($key)
    {
        try
        {
            $data = $this->client->CodePromotion($key);
        }
        catch (\SoapFault $e)
        {
            $data = false;
        }

        return $data;
    }

    /**
     * Retourne l'ical d'une promotion
     *
     * @param $key clé de la promotion
     * @param $nb_weeks int du nombre de semaines à récupérer
     * @param $with array avec les données à inclue en plus
     * @return string|false contenu de l'ical
     */
    public function icalPromotion($key, $nb_weeks = 25, $with = false)
    {
        if( ! is_array($with))
        $with = ["fiAvecCoursAnnules", "fiAvecDateSeances", "fiAvecType", "fiAvecPonderation", "fiAvecMemo", "fiAvecEffectif", "fiAvecSites"];

        $nb_weeks = $this->format_nb_weeks($nb_weeks);

        try
        {
            $ical = $this->client->IcalPromotion($key,
                $nb_weeks,
                $with,
                $this->client->FormatTexteICAL(false, false, true),
                $this->client->FormatTexteICALEnseignant(false, true, true, false, false, false),
                $this->client->FormatTexteICAL(false, false, true),
                $this->client->FormatTexteICAL(true, false, false),
                $this->client->FormatTexteICAL(true, false, false),
                $this->client->FormatTexteICAL(true, false, false),
                $this->client->FormatTexteICAL(false, false, true),
                true, true, true);

        }
        catch(\SoapFault $e)
        {
            $ical = false;
        }

        return $ical;
    }

    /**
     * Retourne les noms des promotions dont la clé est fournis
     *
     * @param $keys array des codes HP
     * @return array|false tableau des noms
     */
    public function nomsTableauDePromotions($keys)
    {
        try
        {
            $data =  $this->client->NomsTableauDePromotions($keys);
        }
        catch (\SoapFault $e)
        {
            $data = false;
        }

        return $data;
    }

    /**
     * Fournit le nom de la promotion dont la clé est envoyé
     * @param $key clé HP de la promotion
     * @return string|false nom de la promotion
     */
    public function nomPromotion($key)
    {
        try
        {
            $data = $this->client->NomPromotion($key);
        }
        catch (\SoapFault $e)
        {
            $data = false;
        }

        return $data;
    }

    /**
     * Retourne les clés de toutes les promotions existantes dans HP
     * @return array<int> Un tableau d'entier(s) qui recense l'intégralité des clés correspondantes aux promotions.
     *
     * Vide si aucune promotion n'existe dans la base.
     */
    public function toutesLesPromotions(): array
    {
        try
        {
            $data = $this->client->ToutesLesPromotions();
        }
        catch (\SoapFault $e)
        {
            $data = false;
        }

        return $data;
    }

    // alias
    public function get_all_promotions(): array
    {
        return $this->ToutesLesPromotions();
    }
}

// ref: https://www.index-education.com/fr/ServiceWeb-Hyperplanning-Promotions.php
