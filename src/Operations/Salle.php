<?php

namespace Dendev\Hpclient\Operations;

use Dendev\Hpclient\Traits\Util;

/**
 * Travail avec les webservices salles d'hyperplanning
 * @see https://www.index-education.com/fr/ServiceWeb-Hyperplanning-Salles.php
 */
trait Salle
{
    use Util;

    /**
     * Récupérer les clés des salles exitantes dans HP
     *
     * @return array|false retourne une un tableau d'integer qui sont les clés des salles
     */
    public function toutesLesSalles(): array
    {
        try
        {
            $data = $this->client->ToutesLesSalles();
        }
        catch (\SoapFault $e)
        {
            $data = false;
        }

        return $data;
    }

    /**
     * Renvoi le nom ( colonne nom dans HP ) de la salle
     * @param $key est la clé utilisé en interne par hp pour identifié la salle
     * @return string|false le nom de la salle
     */
    public function nomSalle($key)
    {
        try
        {
            $data = $this->client->NomSalle($key);
        }
        catch (\SoapFault $e)
        {
            $data = false;
        }

        return $data;
    }

    /**
     * Renvoi le nom de salle dont la clé est fournit
     * @param $keys tableau des clés
     * @return array|false le tableau des noms
     */
    public function nomsTableauDeSalles($keys)
    {
        try
        {
            $data =  $this->client->NomsTableauDeSalles($keys);
        }
        catch (\SoapFault $e)
        {
            $data = false;
        }

        return $data;
    }

    /**
     * Fournit l'ical de la salle
     *
     * @param $key clé de la salle
     * @param $nb_weeks integer nombre de semaines
     * @param $with ajoute ou retire de l'informations dans le resultat
     * @return string|false retourne un ical
     */
    public function icalSalle($key, $nb_weeks = 25, $with = false )
    {
        if( ! is_array($with))
            $with = ["fiAvecCoursAnnules", "fiAvecDateSeances", "fiAvecType", "fiAvecPonderation", "fiAvecMemo", "fiAvecEffectif", "fiAvecSites"];

        $nb_weeks = $this->format_nb_weeks($nb_weeks);

        try
        {
            $ical = $this->client->IcalSalle($key,
                $nb_weeks,
                $with,
                $this->client->FormatTexteICAL(false, false, true),
                $this->client->FormatTexteICALEnseignant(false, true, true, false, false, false),
                $this->client->FormatTexteICAL(false, false, true),
                $this->client->FormatTexteICAL(true, false, false),
                $this->client->FormatTexteICAL(true, false, false),
                $this->client->FormatTexteICAL(true, false, false),
                $this->client->FormatTexteICAL(false, false, true),
                true, true, true);

        }
        catch(\SoapFault $e)
        {
            $ical = false;
        }

        return $ical;
    }

    public function nomToutesLesSalles()
    {
        $locals = $this->client->ToutesLesSalles();
        $names = $this->client->NomsTableauDeSalles($locals);
        return $names;
    }
}
