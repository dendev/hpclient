<?php

namespace Dendev\Hpclient\Soap;

use SoapClient;

class HPConnect
{
    /**
     * Le client qui est utilisé pour l'émission des requêtes au service web Hyperplanning.
     * @see https://www.php.net/manual/fr/class.soapclient.php
     */
    public SoapClient $client;

    public function __construct($wsdl, $login, $password, $location, $trace = true)
    {
        try
        {
            $this->client = new SoapClient($wsdl, array(
                'login' => $login,
                'password' => $password,
                'location' => $location,
                'trace' => $trace,
            ));
            $this->client->__setLocation($wsdl);
        }
        catch (\SoapFault $e)
        {
            return abort(500, "Erreur lors de la création du client SOAP.<br/><br/>Message initial<br/>{$e->getMessage()}<br/><br/>Fichier<br/>{$e->getFile()}");
        }

        return $this;
    }

    public function GetLastRequest()
    {
        return $this->client->__getLastRequest();
    }

    public function GetLastResponse()
    {
        return $this->client->__getLastResponse();
    }

    public function GetLastRequestHeaders()
    {
        return $this->client->__getLastRequestHeaders();
    }

    public function GetLastResponseHeaders()
    {
        return $this->client->__getLastResponseHeaders();
    }
}

// src https://gitlab.com/yorick-dumet/docker-horaires/-/blob/laravel/app/app/Models/Soap/SoapClients/HpSoapClient.php
