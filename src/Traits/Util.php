<?php

namespace Dendev\Hpclient\Traits;

trait Util
{
    /**
     * Transforme la valeur numérique en un tableau utilisable par HP
     * la valeur doit etre entre 0 et 53.
     * Elle définit le nombre de semaines dont on veut récupérer l'information
     * @param $nb_week int nombre de semaines
     * @return array nombre de semaines au format HP
     */
    public function format_nb_weeks($nb_week = 25)
    {
        $formated = [];

        for( $i = 1 ; $i <= $nb_week; $i++ )
        {
            $formated[$i-1] = $i;
        }

        return $formated;
    }
}
